<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Test Mongo DB view
Route::get('mongo', function(Request $request) {
    $collection = Mongo::get()->blog->posts;
    return $collection->find()->toArray();
});

// Blog 
Route::get('blog/{slug}', ['as' => 'blog.detail', 'uses' => 'BlogController@getDetail'])->where('slug', '[\w\d\-\_]+');
Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);
// Route::get('blog', 'BlogController@getIndex');

Route::get('contact', 'PagesController@getContact');
Route::get('about', 'PagesController@getAbout');

// Homepage
Route::get('/', 'PagesController@getIndex');

// Blog Posts CRUD
Route::resource('posts', 'PostController');

// Auth Routes
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home'); 