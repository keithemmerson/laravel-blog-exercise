$(function(){
	$('#carousel-homepage').carousel();


	$('.form-delete').on('submit', function(e){

		if(confirm('Are you sure you want to delete this post?\n\nThis action CANNOT be undone!')){
			return true;
		}else{
			e.preventDefault();
		}
	});
});