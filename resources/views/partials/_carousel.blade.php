<div id="carousel-homepage" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    {{-- <li data-target="#carousel-example-generic" data-slide-to="2"></li> --}}
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="{{ asset('images/image1.jpg') }}" alt="">
      <div class="carousel-caption">
        <h2>Freedom defined</h2>
       <p>Fly like a gannet in the sea</p>
      </div>
    </div>
    <div class="item">
      <img src="{{ asset('images/image2.jpg') }}" alt="">
      <div class="carousel-caption">
        <h2>Sea-lfies</h2>
       <p>Best enjoyed with a mate and choice hand gestures</p>
      </div>
    </div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-homepage" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-homepage" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>