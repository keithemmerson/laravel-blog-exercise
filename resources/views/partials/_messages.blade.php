{{-- Display success messages --}}
@if (Session::has('success'))
	<div class="alert alert-success" role="alert">
		<i class="glyphicon glyphicon-ok"></i> <strong>Success:</strong> {{ session('success') }}
	</div>
@endif

{{-- Display error messages --}}
@if (count($errors) > 0)
	<div class="alert alert-danger" role="alert">
		<i class="glyphicon glyphicon-warning-sign"></i> <strong>Error:</strong>
		<ul>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
		</ul>
	</div>
@endif