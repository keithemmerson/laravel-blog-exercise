@extends('master')

@section('title', '| View Post')

@section('stylesheets')
	<script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=mw0s72z5r7j2fcma0cby5nb2ii5h2ygwmhm06wxuudycfb8u"></script>
	<script>tinymce.init({selector:'textarea'});</script>
@endsection

@section('content')
<div class="row">
    {!! Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) !!}
	<div class="col-md-8">
		<article>

			<div class="form-group">
				{{ Form::label('title', 'Title') }}
				{{ Form::text('title', null, array('class' => 'form-control input-lg' )) }}
			</div>

			<div class="form-group">
				{{ Form::label('slug', 'Slug') }}
				{{ Form::text('slug', null, array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('content', 'Post Content') }}
				{{ Form::textarea('content', null, array('class' => 'form-control')) }}
			</div>

		</article>
	</div>
	<div class="col-md-4">
		<div class="well">
			<dl class="dl post-stats">
				<dt>Url:</dt>
				<dd><a href="{{ route('blog.detail', $post->slug) }}">{{ url($post->slug) }}</a></dd>

				<dt>Date Created:</dt>
				<dd>{{ date('d M Y, g:i:sa', strtotime($post->created_at["date"])) }}</dd>

				<dt>Last Updated:</dt>
				<dd>{{ date('d M Y, g:i:sa', strtotime($post->updated_at["date"])) }}</dd>

			</dl>

			<div class="row">
				<div class="col-sm-6">
					{!! Html::linkRoute('posts.show',
						'Cancel',
						array($post->id),
						array('class' => 'btn btn-danger btn-block') )
					!!}
				</div>
				<div class="col-sm-6">
					{{ Form::submit('Save Changes', array('class' => 'btn btn-success btn-block')) }}
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection