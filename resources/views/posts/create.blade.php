
@extends('master')

@section('title', 'Create Blog Post')

@section('stylesheets')
	<script src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=mw0s72z5r7j2fcma0cby5nb2ii5h2ygwmhm06wxuudycfb8u"></script>
	<script>tinymce.init({selector:'textarea'});</script>
@endsection

@section('content')
    <h1>Create Blog Post</h1>
    <hr>
	<div class="row">
		
		<div class="col-md-8">

	    {{-- Use LaravelCollective Forms --}}
	    {!! Form::open(['route' => 'posts.store']) !!}
		
			{{-- {{ Form::bsText('title') }} --}}

			<div class="form-group">
				{{ Form::label('title', 'Title') }}
				{{ Form::text('title', NULL, array('class' => 'form-control', 'required' => '', 'maxlength' => '255')) }}
			</div>

			<div class="form-group">
				{{ Form::label('slug', 'Slug') }}
				{{ Form::text('slug', NULL, 
					array('class' => 'form-control', 
					      'required' => '', 
					      'minlength' => '5', 
					      'maxlength' => '255'
					)
				) }}
			</div>

			<div class="form-group">
				{{ Form::label('content', 'Post Content') }}
				{{ Form::textarea('content', NULL, array('class' => 'form-control')) }}
			</div>

			{{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg')) }}
		{!! Form::close() !!}

		</div>
	</div>
@endsection