@extends('master')

@section('title', 'All Blog Posts')

@section('content')
    <div class="row">
    	<div class="col-md-10">
    		<h1>All Posts</h1>
    	</div>
    	<div class="col-sm-2">
    		<a href="{{ route('posts.create') }}" class="btn btn-success btn-lg btn-h1-spacing">Create New Post</a>
    	</div>
    </div>
    <table class="table table-hover table-striped">
    	<thead>
    		<tr>
    			{{-- <th>#</th> --}}
    			<th>Title</th>
    			<th>Content</th>
    			<th>Date Created</th>
    			<th></th>
    		</tr>
    	</thead>
    	<tbody>
    	@foreach( $posts as $post)
    		<tr>
    			{{-- <td>{{ $post['$recordId'] }}</td> --}}
    			<td>{{ $post->title }}</td>
    			<td>{{ strip_tags(str_limit($post->content, 80)) }}</td>
    			<td>{{ date('d M Y, g:i:sa', strtotime($post->created_at["date"])) }}</td>
    			<td class="text-right">
    				<div class="btn-group">
    				<a href="{{ route('posts.show', $post->_id) }}" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-eye-open"></i> <span class="sr-only_">View</span></a>
    				<a href="{{ route('posts.edit', $post->_id) }}" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-pencil"></i> <span class="sr-only_">Edit</span></a>
                    </div>

                    <div class="btn-group">
                    {!! Form::open(['route' => ['posts.destroy', $post->_id], 'method' => 'DELETE', 'class' => 'form-delete']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
    				</div>

    			</td>
    		</tr>
    	@endforeach
    	</tbody
@endsection