@extends('master')

@section('title', '| View Post')

@section('content')
<div class="row">
	<div class="col-md-8">
		<article>
		    <h1>{{ $post->title }}</h1>
		    <p>{!! $post->content !!}</p>
		</article>
	</div>
	<div class="col-md-4">
		<div class="well">
		<dl class="dl post-stats">
			<dt>Url:</dt>
			<dd><a href="{{ route('blog.detail', $post->slug) }}">{{ url($post->slug) }}</a></dd>

			<dt>Date Created:</dt>
			<dd>{{ date('d M Y, g:i:sa', strtotime($post->created_at["date"])) }}</dd>

			<dt>Last Updated:</dt>
			<dd>{{ date('d M Y, g:i:sa', strtotime($post->updated_at["date"])) }}</dd>
		</dl>

		<div class="row">
			<div class="col-sm-6">
				{{-- {!! Html::linkRoute('posts.edit',
					'Edit',
					array($post->id),
					array('class' => 'btn btn-primary btn-block') )
				!!} --}}
				
				<a href="{{ route('posts.edit', $post->id) }}" class="btn btn-primary btn-block"><i class="glyphicon glyphicon-pencil"></i> Edit</a>

			</div>
			<div class="col-sm-6">
    			{!! Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'DELETE', 'class' => 'form-delete']) !!}
    			{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-block']) !!}
				{!! Form::close() !!}
			</div>
		</div>
	<hr>
		<a href="{{ route('posts.index') }}" class="btn btn-default btn-block"><i class="glyphicon glyphicon-arrow-left"></i> View all posts</a>
		</div>
	</div>
</div>
@endsection