@extends('master')

@section('title', '| ' . $data['title'])

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-1">
		<article>
			<h1>About this blog</h1>
			<p>My name is {{ $data['fullname'] }} &ndash; Code Cruncher by default, Fish Wrangler by nature.</p>
			<p>If you'd like to get in touch please email me at {{ $data['email'] }}</p>
		</article>
	</div>
</div>
@endsection