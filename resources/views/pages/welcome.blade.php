@extends('master')

@section('title', '| Welcome to this sample Laravel blog')

@section('content')

{{-- Slideshow here maybe or latest blog post --}}
<p></p>


<div class="row">
	<div class="col-md-10 col-md-offset-1">

	@include('partials._carousel')
	<h1>Welcome to this sample blog</h1>
		
	@foreach($posts as $post)

		<div class="entry">
		    <h3>{{ $post->title }}</h3>
		    <p>{{ strip_tags(str_limit($post->content, 150)) }}</p>
		    <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-default">Read more</a>
		</div>

	@endforeach
	</div>

</div>


@endsection