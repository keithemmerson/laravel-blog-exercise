@extends('master')

@section('title', $data['title'])

@section('content')
    <h1>Contact</h1>
    <p><span class="text-muted">&lt;-- contact form here --&gt;</span></p>
    <p>Email me at {{ $data['email'] }}</p>
@endsection