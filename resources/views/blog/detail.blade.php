@extends('master')

@section('title', '| ' . $post->title )

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-1">
		<article>
		    <h1>{{ $post->title }}</h1>
		    <p>{!! $post->content !!}</p>
		</article>
	</div>
</div>
@endsection