@extends('master')

@section('title', '| Blog')

@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		<h1>Blog</h1>

		@foreach ($posts as $post)

		<div class="entry">
			<h3>{{ $post->title }}</h3>
			<h5>Published: {{ date('d M Y', strtotime($post->created_at["date"])) }}</h5>

			<p>{{ substr(strip_tags($post->content), 0, 250) }}{{ strlen(strip_tags($post->content)) > 250 ? '...' : "" }}</p>

			<a href="{{ route('blog.detail', $post->slug) }}" class="btn btn-default">Read More</a>
		</div>
		@endforeach
	</div>
</div>
@endsection