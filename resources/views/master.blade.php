<!doctype html>
<html lang="{{ config('app.locale') }}">
	@include('partials._head')
	<body>

	<header>
	@include('partials._nav')
	</header>

	<section class="container">

		@include('partials._messages')

		@yield('content')
	    
	    @include('partials._footer')

	</section>
	
	@include('partials._javascript')

	@yield('scripts')

	</body>
</html>