<?php

use Illuminate\Database\Seeder;

use MongoDB\Client as Mongo;
// use Purifier;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $collection = (new Mongo)->blog->posts;

        $collection->drop();

		$date       = new DateTime();

		$content = ('<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lobortis, odio sit amet dapibus maximus, arcu purus eleifend purus, ac pharetra lectus libero pretium diam.</p><p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis vulputate nec turpis non faucibus. Proin aliquam tellus non risus elementum, ac rhoncus diam egestas.</p><p>Nunc venenatis ultrices posuere. Quisque tempus laoreet iaculis. Mauris eros metus, tincidunt non tortor vel, ornare posuere quam.</p>');

        // insert dummy blog data into mongodb
        $collection->insertMany([
		    [
				'title'      => 'Blog Post #1',
				'slug'       => 'blog-post-1',
				'content'    => $content,
				'created_at' => $date,
				'updated_at' => $date
		    ],
		    [
				'title'      => 'Blog Post #2',
				'slug'       => 'blog-post-2',
				'content'    => $content,
				'created_at' => $date,
				'updated_at' => $date
		    ],
		    [
				'title'      => 'Blog Post #3',
				'slug'       => 'blog-post-3',
				'content'    => $content,
				'created_at' => $date,
				'updated_at' => $date
		    ],
		    [
				'title'      => 'Blog Post #4',
				'slug'       => 'blog-post-4',
				'content'    => $content,
				'created_at' => $date,
				'updated_at' => $date
		    ],
		    [
				'title'      => 'Blog Post #5',
				'slug'       => 'blog-post-5',
				'content'    => $content,
				'created_at' => $date,
				'updated_at' => $date
		    ],
		]);
    }
}
