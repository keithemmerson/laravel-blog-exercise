<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Mongo;
use Session;

/**
 * Basic controller for handling static pages
 */
class PagesController extends Controller {

	// Homepage
	public function getIndex(){
		
		// get latest posts
        $collection = Mongo::get()->blog->posts;
        $posts = $collection->find(
        	[], 
        	[
        		'sort' => [
        			'created_at' => -1
        		], 
        		'limit' => 5, 
        		'showRecordId' => true
        	]
        );
        // dd($posts);
        // foreach ($posts as $post) {
        // 	// echo '<pre>';
        // 	// print_r($var);
        // 	print_r($post);
        // 	// echo '</pre>';
        // }
        // die();
		// $posts = Post::orderBy('created_at', 'desc')->limit(4)->get();

		return view('pages.welcome')->withPosts($posts);
	}

	// About page
	public function getAbout(){
		
		$data = array(
			"title"    => "About",
			"first"    => "Keith",
			"last"     => "Emmerson",
			"fullname" => "Keith Emmerson",
			"email"    => "keith.emmerson@gmail.com"
		);

		return view('pages.about')->withData($data);
	}

	// Contact page
	public function getContact(){
		
		$data = array(
			"title"    => "Contact Me",
			"email"    => "keith.emmerson@gmail.com"
		);

		return view('pages.contact')->withData($data);
	}

	// Process Contact form
	public function postContact(){

	}
}