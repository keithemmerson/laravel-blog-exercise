<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Mongo;
use Purifier;
use Session;

class PostController extends Controller
{
    /**
     * Create a new controller instance and allow authenticated users only
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all posts
        $collection = Mongo::get()->blog->posts;

        $posts = $collection->find(
            [],
            [
                'sort' => [
                    'created_at' => -1
                ], 
                'limit' => 5,
                'projection' => [
                    '_id' => 1,
                    'title' => 1,
                    'content' => 1,
                    'created_at' => 1,
                ],
                'showRecordId' => true
            ]
        );
        // foreach ($posts as $post) {
        // //     # code...
        //     echo '<br>'.var_dump($post).'<br>';
        //     echo '<br>'.$post->title;
        //     echo '<br> $recordID: '.$post['$recordId'].'<br>';
        // }
        // die();
        // return view to list all blog posts
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate form post data
        $this->validate($request, array(
            'title'   => 'required|max:255',
            'slug'    => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
            'content' => 'required'
        ));
        // store in database as validation successful

        $collection = Mongo::get()->blog->posts;
        $date       = new \DateTime();

        // insert mongodb record
        $insertOneResult = $collection->insertOne([
            'title'      => $request->title,
            'slug'       => $request->slug,
            'content'    => Purifier::clean($request->content),
            'created_at' => $date,
            'updated_at' => $date
        ]);

        // printf("Inserted %d document(s)\n", $insertOneResult->getInsertedCount());

        // get the insertedid and pass to view redirect
        $id = $insertOneResult->getInsertedId();

        // dd($request);
        // die();

        // store success flash session
        $request->session()->flash('success', 'Your blog post was successfully created!');
        // Session:flash('success', 'Your blog post was successfully created!');

        // redirect to another page
        return redirect()->route('posts.show', $id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get mongodb blog posts collection
        $collection = Mongo::get()->blog->posts;
        
        // find specific post from _id
        $post = $collection->findOne(['_id' => new \MongoDB\BSON\ObjectId($id) ]);

        $post->id = $id;
        // var_dump($post);
        // die();

        // render post details view
        return view('posts.show')->withPost($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the post in the db
        // $post = Post::find($id);

        // Get mongodb blog posts collection
        $collection = Mongo::get()->blog->posts;
        
        // find specific post from _id
        $post = $collection->findOne(['_id' => new \MongoDB\BSON\ObjectId($id) ]);

        // assign collection _id to $id var to use in our views
        $post->id = $post->_id;
        
        // render post edit view 
        return view('posts.edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $collection = Mongo::get()->blog->posts;

        // get the post
        // $post = Post::find($id);
        $post     = $collection->findOne(['_id' => new \MongoDB\BSON\ObjectId($id) ]);
        $post->id = $id;

        // validate form post data but not the slug if it hasn't changed
        if ($request->input('slug') == $post->slug) {
            
            $this->validate($request, array(
                'title'   => 'required|max:255',
                'content' => 'required'
            ));

        }else{

            $this->validate($request, array(
                'title'   => 'required|max:255',
                'slug'    => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                'content' => 'required'
            ));
            
        }

        // the record we're updating
        $objectid = new \MongoDB\BSON\ObjectId($id);
        $date     = new \DateTime();

        // update the mongodb post record
        $updateResult = $collection->updateOne(
            ['_id' => $objectid],
            ['$set' => [
                'title'      => $request->input('title'),
                'slug'       => $request->input('slug'),
                'content'    => Purifier::clean($request->input('content')),
                'updated_at' => $date
                ]
            ]
        );

        // printf("Matched %d document(s)\n", $updateResult->getMatchedCount());
        // printf("Modified %d document(s)\n", $updateResult->getModifiedCount());
        // die();

        // store success flash session
        $request->session()->flash('success', 'Your blog post was successfully updated!');

        // redirect to another page
        return redirect()->route('posts.show', $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $collection = Mongo::get()->blog->posts;

        // get the post
        $objectid = new \MongoDB\BSON\ObjectId($id);

        // delete the post record
        $deleteResult = $collection->deleteOne(['_id' => $objectid]);

        // store success flash session
        Session::flash('success', 'The blog post was successfully deleted.');

        // return view to list all blog posts
        return redirect()->route('posts.index');
    }
}
