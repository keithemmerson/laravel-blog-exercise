<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use Mongo;

class BlogController extends Controller
{

	public function getIndex() {
    	
    	$collection = Mongo::get()->blog->posts;

    	$posts = $collection->find(
            [],
            [
                'sort' => [
                    'created_at' => -1
                ], 
                'projection' => [
                    '_id' => 1,
                    'title' => 1,
                    'slug' => 1,
                    'content' => 1,
                    'created_at' => 1,
                ],
                'showRecordId' => true
            ]
        );
		// $posts = Post::paginate(10);
		return view('blog.index')->withPosts($posts);
	}
    public function getDetail($slug){
    	// fetch post record from DB
    	// $post = Post::where('slug', '=', $slug)->first();

    	$collection = Mongo::get()->blog->posts;

    	$post = $collection->findOne(['slug' => $slug ]);

    	// return the view with post object
    	return view('blog.detail')->withPost($post);
    }
}
