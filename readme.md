## Server Requirements

Blog posts are handled with MongoDB and authenticated users with default Laravel / MySQL setup.

- Laravel 5.5
- PHP 7.*
- MySQL
- MongoDB


## Installation

- Clone or copy & paste the repo files into a Laravel project folder.
- Create a `blog` MySQL database and update your MySQL db settings in the /.env file or directly in the /config/database.php file.
- Start your Mongo server with `mongod` command and create a mongo database with `use blog` and create a collection `db.createCollection('posts');
- Run DB migrations `php artisan migrate`
- Run `php artisan db:seed` to import admin user and blog posts
- Run `php artisan serve` 


Browse to http://localhost:8000

## Managing Posts

You can login with the following test admin user credentials keith.emmerson@gmail.com / password